let arr1 = [-3, 8, -11, 0, 2];
let arr2 = [-5, 11, -11, 0, 2];

function filterItems(arr1, arr2) {
  let arr = arr1.concat(arr2);
  const filteredArray = arr.filter((item) => {
    return item > 0;
  });
  return filteredArray;
}

console.log(filterItems(arr1, arr2));
